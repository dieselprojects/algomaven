package com.idanhahn.algoCluster;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.beans.PositionBean;
import com.idanhahn.beans.StockBean;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Types.OPType;

public class Algorithm {

	ArrayList<StockBean> stocks;
	AlgoIF buyAlgo;
	AlgoIF sellAlgo;

	FundsMan funds = FundsMan.getInstance();

	public Algorithm(ArrayList<StockBean> stocks) {
		this.stocks = stocks;
	}

	@SuppressWarnings("unchecked")
	public LinkedList<Bid> start(Calendar date) {

		ArrayList<StockBean>[] gradedStocks = (ArrayList<StockBean>[]) new ArrayList[OPType
				.getSize()];
		for (int i = 0; i < OPType.getSize(); i++)
			gradedStocks[i] = new ArrayList<StockBean>();

		LinkedList<Bid>[] bids = (LinkedList<Bid>[]) new LinkedList[OPType
				.getSize()];
		for (int i = 0; i < OPType.getSize(); i++)
			bids[i] = new LinkedList<Bid>();

		LinkedList<Bid> returnBids = new LinkedList<Bid>();

		Iterator<StockBean> iterator = stocks.iterator();
		while (iterator.hasNext()) {
			StockBean stock = iterator.next();
			buyAlgo = new MacdBBandsAlgo(stock, OPType.BUY);
			sellAlgo = new MacdBBandsAlgo(stock, OPType.SELL);

			// TODO: think of general algorithm approach
			// Add all algorithms here
			((MacdBBandsAlgo) buyAlgo).run();
			((MacdBBandsAlgo) sellAlgo).run();

			gradedStocks[stock.algoResult.getMask()].add(stock);

		}

		// Create buy/sell bid:
		// TODO: think of algorithm for choosing best stock here, currently
		// buy/sell all stocks

		// buy stocks:
		iterator = gradedStocks[OPType.BUY.getMask()].iterator();

		while (iterator.hasNext()) {
			StockBean stock = iterator.next();

			// get amount, currently all:
			if (funds.getAmount(stock.getPrice(), 100) > 0)
				bids[OPType.BUY.getMask()].add(new Bid(stock.ticker, stock
						.getPrice(), funds.getAmount(stock.getPrice(), 100),
						OPType.BUY, date, stock.taIndicators,
						stock.cdlIndicators, stock));

			// Logic for sell
			// Ideas:
			// sell only after some amount of time
			// sell stock which are profitable with no sell indication but a lot
			// of time passed (inside Algorithm)
			// sell only if same buy/Sell indication occur
			// all of those could be indication from different algorithms and
			// just combined here to buy/sell bids

		}

		// sell stocks:
		iterator = gradedStocks[OPType.SELL.getMask()].iterator();

		while (iterator.hasNext()) {
			StockBean stock = iterator.next();
			LinkedList<PositionBean> sellPositions = stock
					.getPositionsForSale();

			// can add some additional logic here to check sell position
			HoldSellAlgo holdSellAlgo = new HoldSellAlgo(stock);
			holdSellAlgo.start();

			if (stock.getAmountPositionsForSale() > 0)
				bids[OPType.SELL.getMask()].add(new Bid(stock.ticker, stock
						.getPrice(), stock.getAmountPositionsForSale(),
						OPType.SELL, date, stock.taIndicators,
						stock.cdlIndicators, stock));
		}

		// Combine all bids:
		for (int i = 0; i < OPType.getSize(); i++) {
			Iterator<Bid> bidIterator = bids[i].iterator();
			while (bidIterator.hasNext()) {
				returnBids.add(bidIterator.next());
				;
			}
		}

		// Stop lost:

		// Get all position from all stocks:
		LinkedList<Bid> stopLostBids = new LinkedList<Bid>();
		iterator = stocks.iterator();
		while (iterator.hasNext()) {
			StockBean stock = iterator.next();
			if (stock.currentStatus) {
				StopLostAlgo stopLostAlgo = new StopLostAlgo(stock);
				Double overRide = stopLostAlgo.start();

				if ((overRide == 1.0) && (stock.getAmountPositionsForSale() > 0))
					stopLostBids.add(new Bid(stock.ticker, stock
							.getPrice(), stock.getAmountPositionsForSale(),
							OPType.SELL, date, stock.taIndicators,
							stock.cdlIndicators, stock));

			}
		}
		returnBids.addAll(stopLostBids);

		return returnBids;

	}

}
