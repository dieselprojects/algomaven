package com.idanhahn.algoCluster;

import java.util.Iterator;

import com.idanhahn.beans.PositionBean;
import com.idanhahn.beans.StockBean;

public class StopLostAlgo implements AlgoIF, Runnable {

	/*
	 * Description, Check if position had lost #% and if so, sell
	 */

	StockBean stock;

	public StopLostAlgo(StockBean stock) {
		this.stock = stock;
	}

	public void run() {
		this.start();
	}

	@Override
	public double start() {

		Double stopLostIndication = 0.0;

		Iterator<PositionBean> iterator = stock.positions.iterator();
		while (iterator.hasNext()) {
			PositionBean position = iterator.next();

			// Stop buy logic:
			// sell only when reaching stop lost
			// sell only when reaching # profit

			if ((position.sell == false)
					&& ((stock.getPrice() / position.buyPrice) < 0.90)) {
				position.sell = true;
				stopLostIndication = 1.0;
			}

		}

		return stopLostIndication;
	}

}
