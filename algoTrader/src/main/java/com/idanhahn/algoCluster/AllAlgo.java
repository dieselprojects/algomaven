package com.idanhahn.algoCluster;

//import com.algoTrader.controllerCluster.OPType;
import org.apache.commons.lang3.ArrayUtils;

import com.idanhahn.beans.StockBean;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.indicatorCluster.BBandsIndicator;
import com.idanhahn.indicatorCluster.CdlIndicators;
import com.idanhahn.indicatorCluster.MACDIndicator;
import com.idanhahn.indicatorCluster.SRSIIndicator;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.SimulateTimeMan;
import com.idanhahn.supportCluster.Types.CDLResultType;
import com.idanhahn.supportCluster.Types.OPType;
import com.idanhahn.supportCluster.Types.TAIndicators;

// execute all algorithms and indicators and return total grade for each stock.
// this class implements runnable to support concurrency grading for multiple 
// stocks

public class AllAlgo implements AlgoIF, Runnable {

	Configuration configuration = Configuration.getInstance();
	Logger logger = Logger.getInstance();
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();

	StockBean stock;
	OPType operationType;
	Double stockGrade = 0.0;

	// ---------------------------- //
	// -- Algo-trader indicators -- //
	// ---------------------------- //
	// TODO:
	// Add algorithms/Indicators here:
	MACDIndicator macdIndicator;
	SRSIIndicator srsiIndicator;
	BBandsIndicator bbandsIndicator;
	CdlIndicators indicatorsAlgo;

	// Indicators results:
	private int macdResult = 0;
	private int srsiResult = 0;
	private int bbandsResult = 0;
	private int[] cdlIndicatorsResult;

	// Indicators grade
	public double macdGrade = 0.0;
	public double srsiGrade = 0.0;
	public double bbandsGrade = 0.0;
	public double cdlIndicatorsGrade = 0.0;

	public AllAlgo(StockBean stock, OPType operationType) {

		this.stock = stock;
		this.operationType = operationType;

		macdIndicator = new MACDIndicator();
		srsiIndicator = new SRSIIndicator();
		bbandsIndicator = new BBandsIndicator();
		indicatorsAlgo = new CdlIndicators();

	}

	public double start() {

		double[] prices = stock.getListAsArray(stock.pastClose);
		double[] openPrices = stock.getListAsArray(stock.pastOpen);
		double[] highPrices = stock.getListAsArray(stock.pastHigh);
		double[] lowPrices = stock.getListAsArray(stock.pastLow);
		double[] closePrices = stock.getListAsArray(stock.pastClose);
		int size = prices.length;
		int beginIndex = 0;
		int endIndex = size - 1;

		ArrayUtils.reverse(prices);
		ArrayUtils.reverse(openPrices);
		ArrayUtils.reverse(highPrices);
		ArrayUtils.reverse(lowPrices);
		ArrayUtils.reverse(closePrices);

		if (configuration.TaIndicators[TAIndicators.MACD.getMask()] == true)
			this.macdResult = macdIndicator.indicate(stock.ticker, prices,
					beginIndex, endIndex, size);

		if (configuration.TaIndicators[TAIndicators.SRSI.getMask()] == true)
			this.srsiResult = srsiIndicator.indicate(stock.ticker, prices,
					beginIndex, endIndex, size);

		if (configuration.TaIndicators[TAIndicators.BBANDS.getMask()] == true)
			this.bbandsResult = bbandsIndicator.indicate(stock.ticker, prices,
					beginIndex, endIndex, size);

		// check minimum daily ticks for indicator base grading:
		if (stock.numOfSamples >= configuration.minDailySamples)
			this.cdlIndicatorsResult = indicatorsAlgo.start(stock,
					operationType);
		else
			logger.addEvent("__ALGO__ not enought daily samples to trade "
					+ stock.ticker);

		// Grades:
		macdGrade = (operationType == OPType.BUY && macdResult == 1) ? 1
				: (operationType == OPType.SELL && macdResult == -1) ? 1 : 0;

		srsiGrade = (operationType == OPType.BUY && srsiResult == 1) ? 1
				: (operationType == OPType.SELL && srsiResult == -1) ? 1 : 0;

		bbandsGrade = (operationType == OPType.BUY && bbandsResult == 1) ? 1
				: (operationType == OPType.SELL && bbandsResult == -1) ? 1 : 0;

		storeAlgoTesterIndicatorTable();

		// TODO: all grading policy here!!!
		for (int i = 0; i < cdlIndicatorsResult.length; i++) {
			if (configuration.cdlIndicatorsCfg.cdlResultType[i] == CDLResultType.BUYSELL) {
				if (operationType == OPType.BUY) {
					if (cdlIndicatorsResult[i] == 1) {
						cdlIndicatorsGrade += cdlIndicatorsResult[i];
					}
				}
				// add grading for indicators such as doji which only produce 1
				if (operationType == OPType.SELL) {
					if (cdlIndicatorsResult[i] == -1) {
						cdlIndicatorsGrade -= cdlIndicatorsResult[i];
					}
				}
			}
			if (configuration.cdlIndicatorsCfg.cdlResultType[i] == CDLResultType.CHANGEDIRECTION) {
				cdlIndicatorsGrade += cdlIndicatorsResult[i];
			}
		}

		if ((operationType == OPType.BUY) && (macdGrade == 1)
				&& (bbandsGrade == 1)) {
			stock.algoResult = operationType;
			this.stockGrade = 1.0;
		}
		if ((operationType == OPType.BUY) && (macdGrade == 1)) {
			stock.algoResult = operationType;
			this.stockGrade = 1.0;
		}

		if ((operationType == OPType.SELL)
				&& ((macdGrade == 1) || (bbandsGrade == 1))) {
			stock.algoResult = operationType;
			// mark position for sell, currently all
			stock.setPositionForSale(stock.getPositionID());
			this.stockGrade = 1.0;
		}

		// this.stockGrade = this.macdGrade + this.srsiGrade + this.bbandsGrade
		// + this.cdlIndicatorsGrade;

		return this.stockGrade;

	}

	public void run() {
		this.start();
	}

	public Double getGrade() {
		return this.stockGrade;
	}

	public void storeAlgoTesterIndicatorTable() {

		// algoTester addition for indicators table:

		// store macd indicator hit
		if (macdResult != 0)
			stock.generalChart.addIndicator(simTimeMan.getTime(), macdResult,
					String.valueOf(TAIndicators.MACD));

		// store srsi indicator hit
		if (srsiResult != 0)
			stock.generalChart.addIndicator(simTimeMan.getTime(), srsiResult,
					String.valueOf(TAIndicators.SRSI));

		// store bbands indicator hit
		if (bbandsResult != 0)
			stock.generalChart.addIndicator(simTimeMan.getTime(), bbandsResult,
					String.valueOf(TAIndicators.BBANDS));

	}

	public String printAlgoResults() {
		String results = "";
		results += "---------------------------\n";
		results += "Ticker: " + stock.ticker + " Grade: " + this.stockGrade
				+ "\n";
		results += "macdGrade: " + this.macdGrade + " srsiGrade: "
				+ this.srsiGrade + " indicatorsGrade: "
				+ this.cdlIndicatorsGrade + "\n";
		return results;
	}

}
