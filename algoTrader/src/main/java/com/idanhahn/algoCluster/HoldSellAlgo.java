package com.idanhahn.algoCluster;

import java.util.Iterator;

import com.idanhahn.beans.PositionBean;
import com.idanhahn.beans.StockBean;

public class HoldSellAlgo implements AlgoIF, Runnable{

	/*
	 * Description, 
	 * check for sell indication when price is lower than buy price and hold sell
	 */
	StockBean stock;
	
	public HoldSellAlgo(StockBean stock){
		this.stock = stock; 
	}

	public void run() {
		this.start();
	}

	public double start() {
		Iterator<PositionBean> iterator = stock.positions.iterator();
		while (iterator.hasNext()){
			PositionBean position = iterator.next();
			
			// Stop buy logic:
			// sell only when reaching stop lost
			// sell only when reaching # profit
			
			if (((position.buyPrice > stock.getPrice()) 
					&& ((stock.getPrice()/position.buyPrice) > 0.95)) ||
					((position.buyPrice < stock.getPrice()) 
							&& (stock.getPrice()/position.buyPrice) < 1.02)
					){
				position.sell = false;
			}
		}
		return 0.0;
	}
	
}
