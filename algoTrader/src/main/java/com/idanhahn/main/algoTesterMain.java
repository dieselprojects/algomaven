package com.idanhahn.main;


import org.neuroph.core.NeuralNetwork;

import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.controllerCluster.OnlineAlgoTrade;
import com.idanhahn.controllerCluster.SimulatedAlgoTrade;
import com.idanhahn.supportCluster.Screener;

public class algoTesterMain {

	public static void main(String[] args) {
	
		NeuralNetwork nn;
		
	    Configuration configuration;
		configuration = Configuration.getInstance();
		Screener screener = new Screener();
		
		System.out.println("----------------------");
		System.out.println("-- Start AlgoTrader --");
		System.out.println("----------------------\n");
        configuration.print();
        
        if (configuration.screenerCfg.useFilterAtStart == true){
            screener.screenStocks();
        }
	
        switch (configuration.tradeType){
        case OFFLINE_SIMULATED:
			System.out.println("Creating simutaltedAlgoTrade");
			SimulatedAlgoTrade simulatedTest = new SimulatedAlgoTrade();
			//RController rController = RController.getInstance();
			//if (rController.startR() == false){
			//	System.out.println("Error inizialing RController");
			//	System.exit(1);
			//}
			System.out.println("Calling simulatedTest.SimulationStarter()");
			simulatedTest.SimulationStarter();
			
			// show chart:
			simulatedTest.dataBaseController.getAllStockBean().get(0).generalChart.showPriceVolumeChart();
			//configuration.simulateCfg.testerArray.get(0).generalChart.printToFile("/home/idanhahn/workarea/git/algotester/sciLab/data");
			//rController.endR();
        	break;
        case ONLINE_SIMULATED:
			System.out.println("Creating OnlineAlgoTrade");
			OnlineAlgoTrade onlineTest = new OnlineAlgoTrade();
			
			System.out.println("Calling onlineTest.SimulationStarter()");
			onlineTest.AlgoStarter(true);
        	break;
        case ONLINE:
        	System.err.println("TODO: add support for real online implementation");
        	System.err.println("Exiting...");
        	System.exit(1);
        	break;
        default:
        	System.err.println("Not valid tradeType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
        	break;
        }
                
		System.out.println("\n----------------------");
		System.out.println("-- Ended AlgoTrader --");
		System.out.println("----------------------\n\n");
		
		while(true);
	}

}
