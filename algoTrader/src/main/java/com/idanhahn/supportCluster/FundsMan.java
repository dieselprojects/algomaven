package com.idanhahn.supportCluster;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import com.idanhahn.beans.PositionBean;
import com.idanhahn.beans.StockBean;
import com.idanhahn.configurationCluster.Configuration;

public class FundsMan {

	// ---------------------------- //
	// - Singleton implementation - //
	// ---------------------------- //

	private static final FundsMan funds = new FundsMan();

	public static FundsMan getInstance() {
		return funds;
	}

	// ------------------- //
	// - Class variables - //
	// ------------------- //

	private Double initialFunds;
	private Double currentFunds;
	private Double investedFunds;
	Configuration configuration;
	Logger logger;
	Double currentInvestedValue = 0.0;
	Double successPosition = 0.0;
	Double failedPosition = 0.0;
	
	
	private FundsMan() {

		// Configuration:
		configuration = Configuration.getInstance();
		System.out.println("__Constructor__ FundsMan, initial Funds: "
				+ configuration.initialFunds);

		// Setting initial Funds
		this.initialFunds = configuration.initialFunds;
		this.currentFunds = configuration.initialFunds;
		this.investedFunds = 0.0;

		// Logger:
		logger = Logger.getInstance();

	}

	// ----------------- //
	// - Class Methods - //
	// ----------------- //

	// purchase stock, by Amount:
	// --------------------------
	public boolean purchaseStock(String ticker, Double price, int amount) {
		if (price * amount > getCurrentFunds()) {

			String errorTransaction = new String(
					"__ERROR__ not enough funds to buy this amount of Stocks, Ticker: "
							+ ticker + ", Price: " + price + ", Amount: "
							+ amount);

			System.err.println(errorTransaction);
			logger.addEvent(errorTransaction);

			return false;

		} else {

			String lastTransaction = new String("__BUY__ Ticker: " + ticker
					+ ", Price: " + price + ", Amount: " + amount);

			setCurrentFunds(getCurrentFunds() - price * amount);
			investedFunds += price * amount;
			logger.addEvent(lastTransaction);

			return true;

		}
	}

	// Sell stocks, By amount:
	// -----------------------
	public boolean sellStock(String ticker, Double price, int amount,
			Double buyPrice) {

		if (buyPrice * amount > investedFunds) {

			String errorTransaction = new String(
					"__ERROR__ sell more than invested funds, Ticker: "
							+ ticker + ", BuyPrice: " + buyPrice + ", Amount: "
							+ amount);
			System.err.println(errorTransaction);
			logger.addEvent(errorTransaction);

			return false;

		} else {

			String lastTransaction;

			setCurrentFunds(getCurrentFunds() + price * amount);
			investedFunds -= buyPrice * amount;

			if (price > buyPrice) {

				lastTransaction = new String("__Sell Profit__ Ticker: "
						+ ticker + ", Price: " + price + ", BuyPrice: "
						+ buyPrice + ", Amount: " + amount + "Total Profit: "
						+ (price - buyPrice) * amount);
				
				successPosition+=1;
				
			} else {

				lastTransaction = new String("__Sell Loss__ Ticker: " + ticker
						+ ", Price: " + price + ", BuyPrice: " + buyPrice
						+ ", Amount: " + amount + "Total Profit: "
						+ (price - buyPrice) * amount);
				
				failedPosition+=1;

			}

			logger.addEvent(lastTransaction);

			return true;

		}
	}

	// Purchase Stocks, By Funds ( will buy as many Stocks )
	// -----------------------------------------------------
	public int purchaseStock(String ticker, Double price, float funds) {
		if (price > getCurrentFunds()) {

			String errorTransaction = new String(
					"__ERROR__ not enough funds to buy minimus amount of Stocks, Ticker: "
							+ ticker + ", Price: " + price + ",Current Funds: "
							+ getCurrentFunds());

			System.out.println(errorTransaction);
			logger.addEvent(errorTransaction);

			return 0;

		} else {

			int amount = (int) (funds / price);

			String lastTransaction = new String("__BUY__ Ticker: " + ticker
					+ ", Price: " + price + ", Amount: " + amount);

			System.out.println(lastTransaction);
			setCurrentFunds(getCurrentFunds() - price * amount);
			investedFunds += price * amount;
			logger.addEvent(lastTransaction);

			return amount;

		}
	}

	// -------------------- //
	// - GetCurrent funds - //
	// -------------------- //

	public Double getCurrentFunds() {
		return currentFunds;
	}

	// -------------------- //
	// - SetCurrent funds - //
	// -------------------- //

	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	// --------------------- //
	// - Get Initial Funds - //
	// --------------------- //

	public Double getInitialFunds() {
		return initialFunds;
	}

	// --------------------- //
	// - Set Initial Funds - //
	// --------------------- //

	public void setInitialFunds(Double initialFunds) {
		this.initialFunds = initialFunds;
	}

	// ------------------- //
	// - Get Total Funds - //
	// ------------------- //

	public Double getTotalFunds() {
		return currentFunds + investedFunds;
	}

	// -------------- //
	// - Get Profit - //
	// -------------- //

	public Double getProfit() {
		return ((currentFunds + currentInvestedValue) / initialFunds - 1.0) * 100.0;
	}

	public int getAmount(double stockPrice, double percentage) {

		if (percentage > 100 || percentage < 0) {
			System.err.println("% error");
			System.exit(1);
		}

		return (int) Math.floor(currentFunds * (percentage / 100) / stockPrice);
	}

	// ------------- //
	// - GetStatus - //
	// ------------- //

	// Returns String with Current Funds, InvestedFunds, and InitialFunds

	public String getStatus(ArrayList<StockBean> stocks) {
		
		Iterator<StockBean> iterator = stocks.iterator();
		while (iterator.hasNext()) {
			StockBean stock = iterator.next();
			Iterator<PositionBean> posIterator = stock.positions.iterator();
			while (posIterator.hasNext()) {
				PositionBean position = posIterator.next();
				currentInvestedValue += stock.getPrice() * position.buyAmount;
			}
		}

		String statusString = "\nFunds Manager, Report:\n";
		statusString += "----------------------\n";
		String profitString = "";

		statusString += "Inital funds: " + initialFunds + "\n";
		statusString += "Current funds: " + currentFunds + "\n";
		statusString += "Invested funds: " + investedFunds + "\n";
		statusString += "CurrentInvestedValue " + currentInvestedValue + "\n";
		statusString += "Total funds: " + currentInvestedValue + currentFunds
				+ "\n";

		profitString = (getProfit() > 0) ? new DecimalFormat("##.##")
				.format(getProfit()) : "("
				+ new DecimalFormat("##.##").format(getProfit()) + ")";
		statusString += "Profit: " + profitString + "%\n";
		
		statusString += "Number of success positions: " + successPosition + "\n";
		statusString += "Number of failed positions: " + failedPosition + "\n";
		statusString += "Success rate: " + (successPosition/(successPosition + failedPosition)) + "\n";

		return statusString;
	}

}
