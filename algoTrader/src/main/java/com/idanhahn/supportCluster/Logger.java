package com.idanhahn.supportCluster;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.idanhahn.beans.StockBean;
import com.idanhahn.beans.TransactionBean;
import com.idanhahn.brokerCluster.BrokerIF;
import com.idanhahn.brokerCluster.SimulatedBroker;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.supportCluster.Types.ReportType;

public class Logger {

	// Logger singleton:
	// -----------------
	private static final Logger LoggerInst = new Logger();

	public static Logger getInstance() {
		return LoggerInst;
	}

	Configuration configuration = Configuration.getInstance();
	BrokerIF broker = SimulatedBroker.getInstance();
	
	
	
	LinkedList<String> logger;
	LinkedList<TransactionBean> transactionQueue;

	private Logger() {

		this.logger = new LinkedList<String>();
		transactionQueue = new LinkedList<TransactionBean>();
		System.out.println("__Constructor__ Logger, Ready to recieve Events");

	}

	public void addEvent(String nextEvent) {
		Calendar currTime = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("DD.MM.YYYY,hh.mm");
		String currTimeStr = sdf.format(currTime.getTime());

		logger.addLast(nextEvent + " | " + currTimeStr);
	}

	public void addTransaction(TransactionBean transactionBean) {
		logger.add(transactionBean.print());
		transactionQueue.add(transactionBean);
	}

	public String print() {
		String loggerString = "";

		if (configuration.reportType != ReportType.REPORT_TRANSACTION) {
			for (String nextLogEvent : logger) {
				loggerString += nextLogEvent + "\n";
				System.out.println(loggerString + "\n");
			}
		}

		loggerString += "-----    TRANSACTION SUMMERY    -----\n";
		for (TransactionBean transaction : transactionQueue) {

			loggerString += transaction.print();

		}

		return loggerString;

	}
	
	
	public String finalReport(ArrayList<StockBean> stocks){
		
		String loggerString = "";
		
		loggerString += FundsMan.getInstance().getStatus(stocks);

		loggerString += "-----    Filtered Stocks Earnings     -----\n";
		for (String ticker : configuration.simulateCfg.testerArray.keySet()) {
			double totalEarning = (configuration.simulateCfg.testerArray.get(ticker).getLast().closePrice
					/configuration.simulateCfg.testerArray.get(ticker).getFirst().closePrice - 1)*100;
			
			loggerString += ticker
					+ ": "
					+ new DecimalFormat(".##").format(totalEarning) + "%\n";
		}
		
		// Calculating success rate:
		
		

		return loggerString;
	}

	// Print all data, Including current funds:
	// ----------------------------------------
	public String printAll(ArrayList<StockBean> stocks) {
		String loggerString = "";

		if (configuration.reportType != ReportType.NO_REPORT) {
			loggerString = print();
		}
		
		loggerString += finalReport(stocks);

		return loggerString;

	}

	// Store all events to logFile, delete events when done:
	// -----------------------------------------------------

	public void storeToLog() {

		// Create String of all events
		String allEventStr = "";
		for (String nextEvent : logger) {
			allEventStr = nextEvent + "\n";
		}

		// Append string to end of log file
		FileWriter writer;
		try {
			writer = new FileWriter(configuration.logFile, true);
			writer.write(allEventStr);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		// delete current events in logger
		logger.clear();
	}

}
