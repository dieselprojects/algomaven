package com.idanhahn.beans;

// Encapsulate time instance
public class TimeInst {
	public long 	timeInMs;
	public boolean dailyTick;
	
	public TimeInst(){
		this.timeInMs 	= 0;
		this.dailyTick 	= false;
	}

	public TimeInst(boolean dailyTick,
			long timeInMs
			){
		this.timeInMs 	= timeInMs ;
		this.dailyTick 	= dailyTick;
	}
}
