package com.idanhahn.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.gui.GeneralChart;
import com.idanhahn.supportCluster.Types.OPType;

public class StockBean implements Comparable<StockBean> {

	Configuration configuration = Configuration.getInstance();

	// ----------------------- //
	// - Main bean variables - //
	// ----------------------- //

	public String ticker;
	public Double price;
	public Integer volume;
	public Calendar lastUpdated;
	public Double grade;
	public Boolean currentStatus = false;// indicate stock bought {true, stock
											// had been bought}
	public OPType algoResult = OPType.NONE;
	public Double buyPrice = 0.0;
	public Double buyGrade = 0.0;
	public Integer investedAmount = 0;

	public LinkedList<PositionBean> positions;
	public Boolean[] taIndicators ;
	public Boolean[] cdlIndicators;
	
	public LinkedList<Double> pastOpen;
	public LinkedList<Double> pastHigh;
	public LinkedList<Double> pastLow;
	public LinkedList<Double> pastClose;
	public LinkedList<Integer> pastVolume;
	public LinkedList<Calendar> pastDates;

	public Integer numOfSamples;

	// Bean Additional Variables:
	public GeneralChart generalChart;
	public HashMap<String, Boolean> indicatorsTable = new HashMap<String, Boolean>();

	// For offlineMode, states the iteration on which the stock had been bought
	public int iterationTrade = 0;

		
	// ---------------- //
	// - Constructors - //
	// ---------------- //

	// Constructor with Tick

	public StockBean(Tick tick) {

		this.ticker = tick.ticker;
		this.price = tick.closePrice;
		this.volume = tick.volume;

		this.grade = 0.0;

		this.pastClose = new LinkedList<Double>();
		this.pastHigh = new LinkedList<Double>();
		this.pastLow = new LinkedList<Double>();
		this.pastOpen = new LinkedList<Double>();
		this.pastVolume = new LinkedList<Integer>();
		this.pastDates = new LinkedList<Calendar>();
		this.positions = new LinkedList<PositionBean>();
		
		this.numOfSamples = 1;

		this.lastUpdated = tick.date;

		this.addTick(tick);
		generalChart = new GeneralChart(this);
	}

	// Complete Constructor:
	// ---------------------
	// TODO: this constructor is being called from data base controller after
	// load file data base,
	// need to edit it each time stock bean is changed, consider different load
	// approach or update
	public StockBean(String ticker, Double grade, Double price, Integer volume,
			Calendar lastUpdated, Boolean currentStatus, Double buyPrice,
			Double buyGrade, Integer investedAmount, Integer pricesSamples,
			Integer numOfSamples, LinkedList<Double> pastClose,
			LinkedList<Double> pastHigh, LinkedList<Double> pastLow,
			LinkedList<Double> pastOpen, LinkedList<Calendar> pastDates) {
		this.ticker = ticker;
		this.grade = grade;
		this.price = price;
		this.volume = volume;
		this.lastUpdated = lastUpdated;
		this.currentStatus = currentStatus;
		this.buyPrice = buyPrice;
		this.buyGrade = buyGrade;
		this.investedAmount = investedAmount;
		this.numOfSamples = numOfSamples;
		this.pastClose = pastClose;
		this.pastHigh = pastHigh;
		this.pastLow = pastLow;
		this.pastOpen = pastOpen;
		this.pastDates = pastDates;
		generalChart = new GeneralChart(this);
	}

	// TODO: maybe implement addTick for accurate dates

	// --------------------------- //
	// - Pricing List convention - //
	// --------------------------- //

	// Price & pastPrices:
	// -------------------
	// Bean prices is arranged by order:
	// Price - Current Price of Stock.
	// past Prices : [0] - Current Price
	// [1] - Last Tick Price
	// [:]
	// [:]
	// [n-1] Oldest Price

	// ----------------------------------------------------------------------------------
	// //

	// pastDate, pastOpen, pastClose:
	// ------------------------------
	// Bean price is arrange by order,
	// each price is a trade day price, open,close, high & low of the lasr trade
	// day
	// past<####> : [0] - last Day trade.
	// [1] - Day before trade
	// [:]
	// [:]
	// [n-1] - oldest trade day available

	// -----------------------------------------------------------------------------------
	// //

	// -------------- //
	// - Add Prices - //
	// -------------- //

	public void addTick(Tick tick) {

		// Check maximum amount of samples reached for daily list
		if (this.numOfSamples >= configuration.maximumNumOfSamples) {

			// remove last prices from all daily past lists and dates, decrement
			// daily dates
			pastClose.removeLast();
			pastHigh.removeLast();
			pastLow.removeLast();
			pastOpen.removeLast();
			pastVolume.removeLast();
			pastDates.removeLast();

			this.numOfSamples--;
		}

		pastOpen.addFirst(tick.openPrice);
		pastHigh.addFirst(tick.highPrice);
		pastLow.addFirst(tick.lowPrice);
		pastClose.addFirst(tick.closePrice);
		pastVolume.addFirst(tick.volume);
		pastDates.addFirst(tick.date);
		this.price = tick.closePrice;
		this.numOfSamples++;

	}

	public double[] getListAsArray(LinkedList<Double> list) {

		double[] outArray = new double[list.size()];

		for (int i = 0; i < list.size(); i++) {
			outArray[i] = (double) list.get(i);
		}

		return outArray;
	}

	public Double getPrice(){
		return pastClose.get(0);
	}
	
	public Double getEarnings() {
		return ((pastClose.getLast() / pastClose.getFirst() - 1) * 100);
	}

	public void setPositionForSale(LinkedList<Integer> idList){
		Iterator<Integer> iterator = idList.iterator();
		while(iterator.hasNext()){
			Integer id = iterator.next();
			for (int i = 0; i < positions.size(); i++){
				if (id == positions.get(i).positionId){
					positions.get(i).sell = true;
				}
			}
		}
	}
	
	public LinkedList<PositionBean> getPositionsForSale(){
		LinkedList<PositionBean> sellPositions = new LinkedList<PositionBean>();
		Iterator<PositionBean> iterator =  positions.iterator();
		while(iterator.hasNext()){
			PositionBean position = iterator.next();
			if (position.sell == true)
				sellPositions.add(position);
		}
		return sellPositions;
	}
	
	
	public Integer getAmountPositionsForSale(){
		Integer sellPositionsAmount = 0;
		Iterator<PositionBean> iterator =  positions.iterator();
		while(iterator.hasNext()){
			PositionBean position = iterator.next();
			if (position.sell == true)
				sellPositionsAmount += position.buyAmount;
		}
		return sellPositionsAmount;
	}
	
	public LinkedList<Integer> getSellPositionID(){
		LinkedList<Integer> positionIDList = new LinkedList<Integer>();
		Iterator<PositionBean> iterator =  positions.iterator();
		while(iterator.hasNext()){
			PositionBean position = iterator.next();
			if (position.sell == true)
				positionIDList.add(position.positionId);
		}
		return positionIDList;
	}
	
	public LinkedList<Integer> getPositionID(){
		LinkedList<Integer> positionIDList = new LinkedList<Integer>();
		Iterator<PositionBean> iterator =  positions.iterator();
		while(iterator.hasNext()){
			PositionBean position = iterator.next();
			positionIDList.add(position.positionId);
		}
		return positionIDList;
	}
	public void removePositions(LinkedList<Integer> idList){
		Iterator<Integer> iterator = idList.iterator();
		while(iterator.hasNext()){
			Integer id = iterator.next();
			for (int i = 0; i < positions.size(); i++){
				if (id == positions.get(i).positionId){
					positions.remove(i);
				}
			}
		}
	}
	
	// -------------------------- //
	// - Print for store & load - //
	// -------------------------- //

	public String print() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");
		String lastUpdatedString = sdf.format(lastUpdated.getTime());

		return ticker + "," + grade + "," + price + "," + volume + ","
				+ lastUpdatedString + "," + "," + currentStatus + ","
				+ buyPrice + "," + buyGrade + "," + investedAmount + ","
				+ numOfSamples;
	}

	public String printLastPrices(LinkedList<Double> pastPrices) {
		String lastPricesString = "";

		for (Double nextPrice : pastPrices) {
			lastPricesString += nextPrice.toString() + ",";
		}

		if (lastPricesString.length() > 0)
			lastPricesString = lastPricesString.substring(0,
					lastPricesString.length() - 1);

		return lastPricesString;

	}

	public String printLastDates(LinkedList<Calendar> pastDates) {

		String lastDatesString = Integer.toString(pastDates.size()) + ",";
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");

		for (Calendar nextDate : pastDates) {
			lastDatesString += sdf.format(nextDate.getTime()) + ",";
		}
		if (lastDatesString.length() > 0)
			lastDatesString = lastDatesString.substring(0,
					lastDatesString.length() - 1);

		return lastDatesString;
	}

	public String printAll() {
		return print() + "||" + printLastPrices(pastClose) + "||"
				+ printLastPrices(pastHigh) + "||" + printLastPrices(pastLow)
				+ "||" + printLastPrices(pastOpen) + "||"
				+ printLastDates(pastDates);
	}

	// Human readable print:
	// ---------------------
	public String printHuman() {
		String outputString = "\nTicker: " + ticker;
		outputString += "\nGrade: " + grade + ", Current Status: "
				+ currentStatus;
		outputString += "\nbuyPrice: " + buyPrice + ", investedAmount: "
				+ investedAmount + ". iterationTrade: " + iterationTrade;
		outputString += "\n----------------------------------------------";

		return outputString;
	}

	public String printHumanWithPrices() {
		String outputString = printHuman();
		outputString += "\nClose Prices: " + printLastPrices(pastClose);
		outputString += "\nHigh Prices: " + printLastPrices(pastHigh);
		outputString += "\nLow Prices: " + printLastPrices(pastLow);
		outputString += "\nOpen Prices: " + printLastPrices(pastOpen);
		return outputString;
	}

	public int compareTo(StockBean bean) {
		return (this.grade >= bean.grade) ? ((this.grade == bean.grade) ? 0 : 1)
				: -1;
	}


}
