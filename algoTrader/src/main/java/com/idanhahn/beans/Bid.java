package com.idanhahn.beans;

import java.util.Calendar;
import java.util.LinkedList;

import com.idanhahn.supportCluster.Types.OPType;

public class Bid {

	// StateMachine approves:
	// ----------------------
	public Boolean approvedHuman;
	public Boolean approvedBroker;

	public String ticker;
	public Double price;
	public int amount;
	public OPType bidType;
	public Calendar date;
	public StockBean stock;
	
	public Boolean[] taIndicators ;
	public Boolean[] cdlIndicators;
	
	public LinkedList<Integer> positionIdList;
	
	public double margin = 0.01; // 1% margin on stock price

	public Bid(String ticker, Double price, int amount, OPType bidType,
			Calendar date, Boolean[] taIndicators, Boolean[] cdlIndicators,StockBean stock) {
		this.ticker = ticker;
		this.price = price;
		this.amount = amount;
		this.bidType = bidType;
		this.date = date;
		this.taIndicators  = taIndicators ;
		this.cdlIndicators = cdlIndicators;

		this.stock = stock;
		
		if (bidType == OPType.SELL)
			this.positionIdList = stock.getPositionID();

		this.approvedHuman = false;
		this.approvedBroker = false;
	}

	public String print() {
		String bidString = "Bid Summery for ticker: " + ticker + "\n";
		bidString += "Request: " + bidType.toString() + ", Price: " + price
				+ ", Amount: " + amount + "\n";
		bidString += "--------------------------------------------\n";
		bidString += "General stock Information:\n";
		bidString += stock.printHuman();

		return bidString;
	}
}
