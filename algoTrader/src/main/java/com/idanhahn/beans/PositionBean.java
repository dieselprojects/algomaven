package com.idanhahn.beans;

import java.util.Calendar;

public class PositionBean {

	public int positionId;
	public Double buyPrice;
	public Integer buyAmount;
	public Calendar buyDate;
	public Boolean[] taIndicators;
	public Boolean[] cdlIndicators;
	public Boolean sell = false;

	public PositionBean(int positionId, Double buyPrice, Integer buyAmount,
			Calendar buyDate, Boolean[] taIndicators, Boolean[] cdlIndicators) {
		this.positionId = positionId;
		this.buyPrice = buyPrice;
		this.buyAmount = buyAmount;
		this.buyDate = buyDate;
		this.taIndicators = taIndicators;
		this.cdlIndicators = cdlIndicators;
	}
}