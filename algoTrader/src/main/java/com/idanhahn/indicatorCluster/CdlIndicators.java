package com.idanhahn.indicatorCluster;

import org.apache.commons.lang3.ArrayUtils;

import com.idanhahn.beans.StockBean;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.supportCluster.SimulateTimeMan;
import com.idanhahn.supportCluster.Types.CDLIndicators;
import com.idanhahn.supportCluster.Types.OPType;

public class CdlIndicators {

	// Use TA-Lib Buy/Bullish Indicators:
	// CDL3INSIDE Three Inside Up // Weak
	// CDL3OUTSIDE Three Outside Up // GOOD
	// CDL3STARSINSOUTH Three Stars In The South // Strongest
	// CDL3WHITESOLDIERS Three Advancing White Soldiers// Strong
	// CDLABANDONEDBABY Abandoned Baby // Strong

	Configuration configuration = Configuration.getInstance();
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();

	@SuppressWarnings("static-access")
	public int[] start(StockBean stock, OPType operationType) {

		// Indicators list:
		CdlGenericIndicator genericIndicator = new CdlGenericIndicator();

		int startIndex = 0;
		int endIndex = stock.pastClose.size() - 1;
		int size = stock.pastClose.size();
		double[] openPrices = stock.getListAsArray(stock.pastOpen);
		double[] highPrices = stock.getListAsArray(stock.pastHigh);
		double[] lowPrices = stock.getListAsArray(stock.pastLow);
		double[] closePrices = stock.getListAsArray(stock.pastClose);

		ArrayUtils.reverse(openPrices);
		ArrayUtils.reverse(highPrices);
		ArrayUtils.reverse(lowPrices);
		ArrayUtils.reverse(closePrices);

		// Three Inside Up:
		int[] cdlResult = new int[configuration.NUM_CDL_INDICATORS];

		for (int i = 0; i < configuration.NUM_CDL_INDICATORS; i++) {
			if (configuration.cdlIndicatorsCfg.cdlIndicators[i] == true) {
				cdlResult[i] = genericIndicator.indicate(
						configuration.cdlIndicatorsCfg.cdlPatternName[i],
						CDLIndicators.values()[i], openPrices, highPrices,
						lowPrices, closePrices, startIndex, endIndex, size);
			}
		}

		// algoTester addition for indicators table:

		for (int j = 0; j < configuration.NUM_CDL_INDICATORS; j++) {
			if (cdlResult[j] != 0) {
				stock.generalChart.addIndicator(simTimeMan.getTime(),
						cdlResult[j],
						configuration.cdlIndicatorsCfg.cdlPatternName[j]);
			}
		}

		return cdlResult;
	}

}
