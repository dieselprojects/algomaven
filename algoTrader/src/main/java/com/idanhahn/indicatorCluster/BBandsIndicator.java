package com.idanhahn.indicatorCluster;

import com.idanhahn.configurationCluster.MACDCfg;
import com.idanhahn.supportCluster.Logger;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class BBandsIndicator {

	Logger logger = Logger.getInstance();
	
	RetCode bbandsRCode;
	Core bbands = new Core();

	MInteger outBegIdx;
	MInteger outNBElement;
	double[] outUpper;
	double[] outMid;
	double[] outLower;
	int result = 0;

	public int indicate(String ticker, double[] prices, int startIndex,
			int endIndex, int size) {

		RetCode bbandsRCode;
		Core bbands = new Core();

		outBegIdx = new MInteger();
		outNBElement = new MInteger();
		outUpper = new double[size];
		outMid = new double[size];
		outLower = new double[size];

		bbandsRCode = bbands.bbands(startIndex, endIndex, prices, 20, 2, 2,
				MACDCfg.macdMATypeFastParam, outBegIdx, outNBElement, outUpper,
				outMid, outLower);

		if (bbandsRCode != RetCode.Success)
			System.err.println("BBANDS failed on " + ticker);

		if (outNBElement.value > 0) {
			result = bbandsCustomStratergy2(prices, size);
		}

		return result;
	}

	private int bbandsDefaultStrategy(double[] prices, int size) {

		return (prices[size - 1] < outLower[size - outBegIdx.value - 1]) ? 1
				: (prices[size - 1] > outUpper[size - outBegIdx.value - 1]) ? -1
						: 0;

	}

	private int bbandsCustomStratergy1(double[] prices, int size) {

		int BBANDS_LOOK_BACK = 7;
		int result = 0;

		if (outNBElement.value > BBANDS_LOOK_BACK) {
			for (int i = 0; i < BBANDS_LOOK_BACK; i++) {
				if (prices[size - 1 - i] < outLower[size - outBegIdx.value - 1
						- i]) {
					result = 1;
				} else if (prices[size - 1 - i] > outUpper[size
						- outBegIdx.value - 1 - i]) {
					result = -1;
				}
			}
		}
		return result;
	}

	private int bbandsCustomStratergy2(double[] prices, int size) {

		int BBANDS_LOOK_BACK = 3;
		double MARGIN = 0.05; // 5% margin
		int result = 0;
		int index = size - outBegIdx.value - 1;

		if (outNBElement.value > BBANDS_LOOK_BACK) {
			for (int i = 0; i < BBANDS_LOOK_BACK; i++) {

				if (prices[size - 1 - i] < 
					(outLower[index - i] + (outUpper[index - i] - outLower[index - i])*MARGIN)){

					result = 1;

				} else if (prices[size - 1 - i] > 
					(outUpper[index - i] - (outUpper[index - i] - outLower[index - i])*MARGIN)){

					result = -1;

				}

			}
		}
		return result;
	}
}
