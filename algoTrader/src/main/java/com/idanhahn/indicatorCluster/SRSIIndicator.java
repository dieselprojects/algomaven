package com.idanhahn.indicatorCluster;

import java.util.Arrays;

import com.idanhahn.configurationCluster.SRSICfg;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/*
 * SRSI definition
 *
 * Buy - when SRSI crosses above oversold line (30)
 * Sell - when SRSI crosses below overbought line (70)
 * 
 */

public class SRSIIndicator {

	public int indicate(String ticker, double[] prices, int startIndex,
			int endIndex, int size) {

		RetCode srsiCode;
		Core srsi = new Core();

		MInteger outBegIdx = new MInteger();
		MInteger outNBElement = new MInteger();
		double[] outSRSIK = new double[size];
		double[] outSRSID = new double[size];
		int result = 0;

		SRSICfg.getInstance();

		srsiCode = srsi.stochRsi(startIndex, endIndex, prices, (int) (-4e+37),
				SRSICfg.srsiFastKParam, SRSICfg.srsiFastDParam,
				SRSICfg.srsiMATypeDParam, outBegIdx, outNBElement, outSRSIK,
				outSRSID);

		if (srsiCode != RetCode.Success)
			System.err.println("SRSI indicator failed for " + ticker);

		// check data validity
		if (outNBElement.value > 0)
			result = (outSRSIK[size - outBegIdx.value - 1] < SRSICfg.srsiBuyParam) ? 1
					: (outSRSIK[size - outBegIdx.value - 1] > SRSICfg.srsiSellParam) ? -1
							: 0;

		System.out.println("prices = " + Arrays.toString(prices));
		System.out.println("srsik = " + Arrays.toString(outSRSIK));
		System.out.println("srsik[" + (size - outBegIdx.value - 1) + "] = "
				+ outSRSIK[size - outBegIdx.value - 1]);
		System.out.println("srsid = " + Arrays.toString(outSRSID));
		System.out.println("srsid[" + (size - outBegIdx.value - 1) + "] = "
				+ outSRSID[size - outBegIdx.value - 1]);
		System.out.println("outBegIdx   = " + outBegIdx.value);
		System.out.println("outNBElement = " + outNBElement.value);

		return result;

	}

}
