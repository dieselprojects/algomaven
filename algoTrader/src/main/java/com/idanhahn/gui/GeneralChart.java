package com.idanhahn.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import com.idanhahn.beans.Bid;
import com.idanhahn.beans.StockBean;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.SimulateTimeMan;
import com.idanhahn.supportCluster.Types.OPType;

public class GeneralChart extends JPanel {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	Configuration configuration = Configuration.getInstance();
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	Logger logger = Logger.getInstance();
	
	
	StockBean stock;
	String title;

	TimeSeries priceGraph;
	TimeSeriesCollection priceDataSet;
	TimeSeries volumeGraph;
	TimeSeriesCollection volumeDataSet;
	TimeSeries buyGraph = new TimeSeries("buy");
	TimeSeriesCollection buyDataSet = new TimeSeriesCollection();
	TimeSeries sellGraph = new TimeSeries("sell");
	TimeSeriesCollection sellDataSet = new TimeSeriesCollection();
	DefaultListModel<String> buySellList = new DefaultListModel<String>();

	DefaultListModel<String> indicatorsBuySell = new DefaultListModel<String>();
	LinkedList<IndicatorType> indicatorList = new LinkedList<IndicatorType>();

	LinkedList<Double> buyList = new LinkedList<Double>();
	LinkedList<Integer> buyListIndex = new LinkedList<Integer>();
	LinkedList<Double> sellList = new LinkedList<Double>();
	LinkedList<Integer> sellListIndex = new LinkedList<Integer>();

	@SuppressWarnings({ "unchecked", "static-access" })
	public LinkedList<Date>[] indicatorsBuyList = new LinkedList[configuration.TOTAL_NUM_INDICATORS];
	@SuppressWarnings({ "unchecked", "static-access" })
	public LinkedList<Date>[] indicatorsSellList = new LinkedList[configuration.TOTAL_NUM_INDICATORS];

	public GeneralChart(StockBean stock) {

		this.title = stock.ticker;
		this.stock = stock;
		// stock price:
		priceGraph = new TimeSeries(stock.ticker);

		priceDataSet = new TimeSeriesCollection();
		priceDataSet.addSeries(priceGraph);

		volumeGraph = new TimeSeries(stock.ticker);

		for (int i = 0; i < configuration.simulateCfg.getEndOfTest(); i++) {
			Day day = new Day(configuration.simulateCfg.testerArray.get(title)
					.get(i).date.getTime());
			priceGraph.add(day, configuration.simulateCfg.testerArray
					.get(title).get(i).closePrice);
			volumeGraph
					.add(day, configuration.simulateCfg.testerArray.get(title)
							.get(i).volume);
		}

		volumeDataSet = new TimeSeriesCollection();
		volumeDataSet.addSeries(volumeGraph);

		for (int i = 0; i < configuration.TOTAL_NUM_INDICATORS; i++) {
			indicatorsBuyList[i] = new LinkedList<Date>();
			indicatorsSellList[i] = new LinkedList<Date>();
		}
	}

	public JFreeChart buildPriceVolumeChart(String title) {
		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, "Date",
				"Price", priceDataSet, true, true, false);
		chart.setBackgroundPaint(Color.white);
		XYPlot plot = chart.getXYPlot();

		// price:
		NumberAxis rangeAxis1 = (NumberAxis) plot.getRangeAxis();
		rangeAxis1.setLowerMargin(0.40); // to leave room for volume bars
		DecimalFormat format = new DecimalFormat("00.00");
		rangeAxis1.setNumberFormatOverride(format);

		XYItemRenderer renderer1 = plot.getRenderer();
		renderer1.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")));

		// Buy/Sell dot scatter chart
		// Buy:
		if (!buyGraph.isEmpty()) {
			buyDataSet.addSeries(buyGraph);
			XYItemRenderer rendererBuy = new XYLineAndShapeRenderer(false, true);
			rendererBuy.setBasePaint(Color.green);
			rendererBuy.setBaseItemLabelGenerator(new SeriesLabelGen());
			rendererBuy.setBaseItemLabelPaint(Color.GREEN);
			rendererBuy.setBaseItemLabelsVisible(true);
			plot.setRenderer(2, rendererBuy);
			plot.setDataset(2, buyDataSet);
			plot.mapDatasetToRangeAxis(1, 2);
		}
		// Sell:
		if (!sellGraph.isEmpty()) {
			sellDataSet.addSeries(sellGraph);
			XYItemRenderer rendererSell = new XYLineAndShapeRenderer(false,
					true);
			rendererSell.setBasePaint(Color.RED);
			rendererSell.setBaseItemLabelGenerator(new SeriesLabelGen());
			rendererSell.setBaseItemLabelPaint(Color.MAGENTA);
			rendererSell.setBaseItemLabelsVisible(true);

			plot.setRenderer(3, rendererSell);
			plot.setDataset(3, sellDataSet);
			plot.mapDatasetToRangeAxis(1, 3);
			plot.getDomainAxis().setTickLabelPaint(new Color(0, 100, 0, 0));
		}

		// Volume:
		NumberAxis rangeAxis2 = new NumberAxis("Volume");
		rangeAxis2.setUpperMargin(1.00); // to leave room for price line
		plot.setRangeAxis(1, rangeAxis2);
		plot.setDataset(1, volumeDataSet);
		plot.mapDatasetToRangeAxis(1, 1);
		XYBarRenderer renderer2 = new XYBarRenderer(0.20);
		renderer2.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				StandardXYToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT,
				new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat(
						"0,000.00")));
		plot.setRenderer(1, renderer2);

		new LinkedList<Double>();
		new LinkedList<Integer>();

		for (int i = 0; i < indicatorList.size(); i++) {

			ValueMarker marker = new ValueMarker(priceDataSet.getXValue(0,
					indicatorList.get(i).index));

			if (indicatorList.get(i).opType == OPType.BUY)
				marker.setPaint(Color.GREEN);
			else
				marker.setPaint(Color.RED);

			marker.setLabel(new String(i + " "
					+ String.valueOf(indicatorList.get(i).indicator)));
			marker.setLabelAnchor(RectangleAnchor.CENTER);
			marker.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
			plot.addDomainMarker(marker);

		}

		return chart;
	}


	public void showPriceVolumeChart() {

		this.setLayout(new GridLayout(2, 1));

		// Top row:
		this.add(new ChartPanel(buildPriceVolumeChart(title)),
				BorderLayout.PAGE_START);

		// Bottom row:
		JPanel bottomRow = new JPanel(new GridLayout(1, 3));

		// BuySell ( 2X1 ):
		JScrollPane listScroller = new JScrollPane(generateBuySellTable()); // JTABLE
		bottomRow.add(listScroller, BorderLayout.WEST);

		// Indicators triggered:
		JScrollPane indicatorTable = new JScrollPane(generateIndicatorsTable());
		bottomRow.add(indicatorTable, BorderLayout.CENTER);

		// Configuration (2X3)
		JScrollPane cfgListScroller = generateConfigurationTable();
		bottomRow.add(cfgListScroller, BorderLayout.EAST);

		this.add(bottomRow);

		// Frame:
		JFrame frame = new JFrame(title + " Price/Volume");
		frame.getContentPane().add(this, BorderLayout.CENTER);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screenSize.width, screenSize.height);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public JTable generateBuySellTable() {
		String[] columnNames = { "Index", "Date", "Op", "Price", "Amount",
				"Profit" };
		String[][] tableData = new String[buySellList.size()][columnNames.length];
		int indexBuy = 0;
		int indexSell = 0;
		OPType[] buySellArr = new OPType[buySellList.size()];

		for (int i = 0; i < buySellList.size(); i++) {
			String[] splitedString = buySellList.getElementAt(i).split(" ");
			tableData[i][0] = (splitedString[1].equals("BUY")) ? String
					.valueOf(indexBuy++) : String.valueOf(indexSell++);
			if (splitedString[1].equals("BUY")) {
				buySellArr[i] = OPType.BUY;
			} else {
				buySellArr[i] = OPType.SELL;
			}
			for (int j = 0; j < columnNames.length - 1; j++)
				tableData[i][j + 1] = splitedString[j];
		}

		JTable returnedTable = new JTable(tableData, columnNames);
		CustomCellRenderer tableRenderer = new CustomCellRenderer();
		tableRenderer.bidArray = buySellArr;

		for (int i = 0; i < columnNames.length; i++) {
			returnedTable.getColumnModel().getColumn(i)
					.setCellRenderer(tableRenderer);
		}

		return returnedTable;

	}

	public JTable generateIndicatorsTable() {

		String[] columnNames = { "Index", "Date", "Op", "Indicator" };
		String[][] tableData = new String[indicatorsBuySell.size()][columnNames.length];
		int index = 0;
		OPType[] buySellArr = new OPType[indicatorsBuySell.size()];

		for (int i = 0; i < indicatorsBuySell.size(); i++) {
			String[] splitedString = indicatorsBuySell.getElementAt(i).split(
					" ");
			tableData[i][0] = String.valueOf(index++);
			if (splitedString[1].equals("BUY")) {
				buySellArr[i] = OPType.BUY;
			} else {
				buySellArr[i] = OPType.SELL;
			}
			for (int j = 0; j < columnNames.length - 1; j++)
				tableData[i][j + 1] = splitedString[j];
		}
		JTable returnedTable = new JTable(tableData, columnNames);
		CustomCellRenderer tableRenderer = new CustomCellRenderer();
		tableRenderer.bidArray = buySellArr;

		for (int i = 0; i < columnNames.length; i++) {
			returnedTable.getColumnModel().getColumn(i)
					.setCellRenderer(tableRenderer);
		}
		return returnedTable;
	}

	public JScrollPane generateConfigurationTable() {

		// Create list data:
		DefaultListModel<String> cfgListModel = new DefaultListModel<String>();

		LinkedList<String> allCfgElements = configuration.getValues();
		
		// Get test results:
		
		// final test results
		// Stock performance
		// Success rates
	
		ArrayList<StockBean> stockList = new ArrayList<StockBean>();
		stockList.add(stock);
		
		String finalResultString = logger.finalReport(stockList);
		String[] finalResultArr = finalResultString.split("\\n");
		
		cfgListModel.addElement("Number of success positions");
		
		
		
		
		cfgListModel.addElement(" ");
		cfgListModel.addElement(" ");
		cfgListModel.addElement(" ");
		cfgListModel.addElement(" ");
		for (String nextString : finalResultArr){
			cfgListModel.addElement(nextString);
		}
		
		for (String cfgElement : allCfgElements) {
			cfgListModel.addElement(cfgElement);
		}

		JList<String> cfgList = new JList<String>(cfgListModel);

		cfgList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		JScrollPane listScroller = new JScrollPane(cfgList);

		return listScroller;
	}

	private class CustomCellRenderer extends DefaultTableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		OPType[] bidArray;

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {

			Component rendererComp = super.getTableCellRendererComponent(table,
					value, isSelected, hasFocus, row, column);

			if (bidArray[row].equals(OPType.BUY)) {
				rendererComp.setBackground(Color.GREEN);
			} else {
				rendererComp.setBackground(Color.RED);
			}

			return rendererComp;
		}

	}

	private class SeriesLabelGen extends StandardXYItemLabelGenerator {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public String generateLabel(XYDataset dataset, int series, int item) {

			return String.valueOf(item);
		}
	}

	public void printToFile(String fileName) {
		try {
			PrintWriter writer = new PrintWriter(fileName, "ascii");

			// Prices:
			String prices = new String();
			for (Double price : stock.pastClose) {
				prices += " " + String.valueOf(price);
			}

			// Date:
			String dates = new String();
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			for (Calendar date : stock.pastDates) {
				dates += " " + sdf.format(date.getTime());
			}

			// Buy:
			String buys = new String();
			String buysIndex = new String();
			for (int i = 0; i < buyList.size(); i++) {
				buys += " " + String.valueOf(buyList.get(i));
				buysIndex += " " + String.valueOf(buyListIndex.get(i));
			}

			// Sell:
			String sells = new String();
			String sellsIndex = new String();
			for (int i = 0; i < sellList.size(); i++) {
				sells += " " + String.valueOf(sellList.get(i));
				sellsIndex += " " + String.valueOf(sellListIndex.get(i));
			}

			writer.println(prices);
			writer.println(dates);
			writer.println(buys);
			writer.println(buysIndex);
			writer.println(sells);
			writer.println(sellsIndex);
			writer.close();

		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private class IndicatorType {

		public Integer index;
		public String indicator;
		public OPType opType;

		public IndicatorType(Integer index, String indicator, OPType opType) {
			this.index = index;
			this.indicator = indicator;
			this.opType = opType;
		}

	}

	// Add buy sell point
	public void addTransaction(Bid bid, Calendar date) {

		Day day = new Day(date.getTime());
		double profit = 0.0;
		if (bid.bidType == OPType.BUY) {
			buyGraph.add(day, bid.price);
		} else {
			sellGraph.add(day, bid.price);
			profit = bid.price
					* bid.amount
					- (double) buyGraph.getDataItem(
							buyGraph.getItems().size() - 1).getValue()
					* bid.amount;

		}
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		// TODO: add additional information regarding buy and sell
		buySellList.addElement(new String(df.format(date.getTime()) + " "
				+ bid.bidType + " " + bid.price + " " + bid.amount + " "
				+ profit));
	}

	// Add Indicator point
	public void addIndicator(Calendar date, int type, String indicator) {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		indicatorsBuySell.addElement(new String(df.format(date.getTime()) + " "
				+ ((type == 1) ? OPType.BUY : OPType.SELL) + " " + indicator));

		// Add index / indicator type / operation to Indicator list for plot
		indicatorList.add(new IndicatorType(simTimeMan.iteration, indicator,
				(type == 1) ? OPType.BUY : OPType.SELL));

	}

	public void addTransactionMatlab(Bid bid, int iteration) {
		if (bid.bidType == OPType.BUY) {
			buyList.add(bid.price);
			buyListIndex.add(iteration);
		} else {
			sellList.add(bid.price);
			sellListIndex.add(iteration);
		}
	}
}
