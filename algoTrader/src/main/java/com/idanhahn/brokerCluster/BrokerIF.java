package com.idanhahn.brokerCluster;

import java.util.ArrayList;
import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.beans.Tick;


public interface BrokerIF {
	
	
	// ---------------------------- //
	// - Get Tick (single ticker) - //
	// ---------------------------- //
	
	public Tick getTick(String ticker);
	
	
	// ------------------------------- //
	// - Get Tick (By ticker vector) - //
	// ------------------------------- //
	
	// Call Broker and Get Tick information: PriceTick - Current price tick Information, 
	// Daily Tick Last Trade Day prices Information
	// Input: ArrayList<String> tickers
	// Output: ArrayList<DailyTick>
	
	public LinkedList<Tick> getTicks(ArrayList<String> tickerList);
	
	
	// ------------- //
	// - Place Bid - //
	// ------------- //
	
	// Place Single bid
	public Bid placeBid(Bid bid);
	
	// -------------- //
	// - Place Bids - //
	// -------------- //
	
	// Place arrayList of bids
	public LinkedList<Bid> placeBids(LinkedList<Bid> bids);
	
	
	// --------------------- //
	// - Get StocksTickers - //
	// --------------------- //
	
	public ArrayList<String> getAllTickers(Boolean getTickersFromFile);
	
	
	
}
