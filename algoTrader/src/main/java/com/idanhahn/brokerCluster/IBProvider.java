package com.idanhahn.brokerCluster;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.ib.contracts.StkContract;
import com.ib.controller.ApiConnection.ILogger;
import com.ib.controller.ApiController;
import com.ib.controller.ApiController.IConnectionHandler;
import com.ib.controller.ApiController.IHistoricalDataHandler;
import com.ib.controller.Bar;
import com.ib.controller.NewContract;
import com.ib.controller.Types.BarSize;
import com.ib.controller.Types.DurationUnit;
import com.ib.controller.Types.WhatToShow;
import com.idanhahn.beans.Bid;
import com.idanhahn.beans.Tick;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.supportCluster.FundsMan;


public class IBProvider implements OnlineProvider, IConnectionHandler{
	Configuration configuration = Configuration.getInstance();
	ApiController twsController;
	FundsMan funds = FundsMan.getInstance();

	private class twsLogger implements ILogger{

		@Override
		public void log(String valueOf) {
			// TODO Auto-generated method stub
		}
		
	}
	
	twsLogger inLogger = new twsLogger();
	twsLogger outLogger = new twsLogger();
		
	public IBProvider() {
		// TODO Auto-generated constructor stub
		twsController = new ApiController(this, inLogger, outLogger);
		twsController.connect(configuration.twsHost,configuration.twsPort,configuration.twsID);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Tick> requestPrices(String[] tickers) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bid placebid(Bid bid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedList<Tick> getPastTicks(String ticker, String startDate,
			String endDate, String sample) throws IOException, ParseException {
	
		// Calculate number of days:
		
		
		
		
		// From get historical data:
		StkContract contract = new StkContract("AAPL");
		//contract.m_strike = 0;
		//contract.m_expiry = "";
		contract.m_primaryExch = "NASDAQ";
		//contract.m_secType = SecType.STK.toString();
		historicalDataHandler histDataHandler = new historicalDataHandler();
		NewContract newContract = new NewContract(contract);
		System.out.println(newContract.description());
		twsController.reqHistoricalData(newContract, "20150301 12:00:00", 1, DurationUnit.MONTH, BarSize._1_day, WhatToShow.TRADES, true, histDataHandler);
		
		
		
		return null;
	}

	@Override
	public void connected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void accountList(ArrayList<String> list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void error(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void message(int id, int errorCode, String errorMsg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show(String string) {
		// TODO Auto-generated method stub
		
	}
	
	private class historicalDataHandler implements IHistoricalDataHandler{

		@Override
		public void historicalData(Bar bar, boolean hasGaps) {
			// TODO Auto-generated method stub
			System.out.println("Inside historical data handler - Data");
		}

		@Override
		public void historicalDataEnd() {
			// TODO Auto-generated method stub
			System.out.println("Inside historical data handler - End");
			
		}
		
	}

}
