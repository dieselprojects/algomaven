package com.idanhahn.brokerCluster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.beans.Tick;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.supportCluster.Logger;

public class TWSBroker implements BrokerIF {

	private static final TWSBroker broker = new TWSBroker();
	
	public static TWSBroker getInstance(){
		return broker;
	}
	
	private OnlineProvider onlineProvider;
	
	private Configuration configuration = Configuration.getInstance();
	private Logger logger = Logger.getInstance();
	
	// --------------- //
	// - Constructor - //
	// --------------- //	
	
	private TWSBroker(){
		
		System.out.println("__Constructor__ TWS Broker");
		switch (configuration.providerType){
		case YAHOO_PROVIDER:
			System.out.println("Creating onlineProvider, providerType - Yahoo provider");
			onlineProvider =  new YahooProvider();
			break;
		case IB_PROVIDER:
			System.out.println("Creating onlineProvider, providerType - IB provider");
			onlineProvider =  new IBProvider();
			break;
			
		default:
			System.err.println("Invalid provider type selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}
	
		if (configuration.simulatedSetupPhase == true){
			
			try {
				onlineProvider.getPastTicks("AAPL", null, null, null);
			} catch (IOException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		
		logger.addEvent("__Constructor__ Online Broker, using online provider - " + onlineProvider.getName());		
	
	}

	public Tick getTick(String ticker) {
		// TODO Auto-generated method stub
		return null;
	}

	public LinkedList<Tick> getTicks(ArrayList<String> tickerArray){
		// TODO Auto-generated method stub
		return null;
	}

	public Bid placeBid(Bid bid) {
		// TODO Auto-generated method stub
		return null;
	}

	public LinkedList<Bid> placeBids(LinkedList<Bid> bids) {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<String> getAllTickers(Boolean getTickersFromFile) {
		
		ArrayList<String> tickerArray = new ArrayList<String>();
		
		if (getTickersFromFile == true){
			
			// Loading Filtered stock list and storing in tickerArray
			try{
				
				BufferedReader FilteredReader = new BufferedReader(new FileReader(configuration.filteredStockFile));
				String tickerString = "";
			
				while ( (tickerString = FilteredReader.readLine()) != null){
								
					tickerString = tickerString.replaceAll("\"", "");
					tickerArray.add(tickerString);

				}
				FilteredReader.close();	
			
			} catch( Exception e ){
				e.printStackTrace();
				System.exit(1);
			}
			
		} else{
			System.err.println("Should not get tickers from broker! ");
			System.exit(1);
		}

		return tickerArray;
	}
}
