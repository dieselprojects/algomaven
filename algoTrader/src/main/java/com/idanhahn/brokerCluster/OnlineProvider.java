package com.idanhahn.brokerCluster;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.beans.Tick;

public interface OnlineProvider {
	

	public String OPname = "";
	
	public String getName();
	
	// ------------------ //
	// - Request Prices - //
	// ------------------ //
	
	public ArrayList<Tick> requestPrices(String[] tickers);
	
	
	// ------------- //
	// - Place Bid - //
	// ------------- //
	
	public Bid placebid(Bid bid);
	

	// ---------------- //
	// - getPastTicks - //
	// ---------------- //
	
	public LinkedList<Tick> getPastTicks(String ticker, String startDate, String endDate, String sample) throws IOException, ParseException;

}
