package com.idanhahn.confirmation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.gui.DialogHumanConfirmManual;


public class WindowHumanConfirm implements HumanIF, ActionListener{

	
	public Bid getConfirmation(Bid requestBid)  {
		
		Bid returnedBid = requestBid;
		// Open window GUI with the requested BID and wait for confirmation or denial

	
		
		
		DialogHumanConfirmManual nextWindowGUI = new DialogHumanConfirmManual(requestBid);

		
		// Wait on event
		
		while ((returnedBid = nextWindowGUI.getAnswer()) == null )
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Catched Exception - " + e.getLocalizedMessage());
				System.exit(1);
			}
		

		System.out.println("Got Bid " + returnedBid.print());
		
		return returnedBid;

	}


	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList) {
		
		LinkedList<Bid> approvedBids = new LinkedList<Bid>();
		
		for(Bid nextBid : bidsList){
			
			Bid returnedBid = getConfirmation(nextBid);
			
			if (returnedBid.approvedHuman == true){
				approvedBids.addLast(returnedBid);
			}
		
		}
		
		return approvedBids;	
	}



	public void actionPerformed(ActionEvent e) {

		
	}

	
	
	
}
