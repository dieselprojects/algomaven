package com.idanhahn.confirmation;

import java.util.LinkedList;

import com.idanhahn.beans.Bid;
import com.idanhahn.supportCluster.webConnection;

public class webConfirm implements HumanIF {

	
	public Bid getConfirmation(Bid requestBid) {
		
		// Use webConnection to send Confirm request, don't wait for individual response
		
		// webConnection to table Pending add send MyAQL statement 
		
		// TODO: need singleton
		webConnection webConnection = new webConnection();
		
		webConnection.sendBidRequestMySQL(requestBid);
		
		
		
		// TODO Auto-generated method stub
		return null;
	}


	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList) {
		
		// Send all outstanding BidRequest
		// Save Number of request
		
		int numberOfOutStandingBids = bidsList.size();
		
		for(Bid nextBid : bidsList){
			getConfirmation(nextBid);
		}
		
		
		// perform polling on MySQL EXE table size until receives all responses
		// return Responses
		webConnection webConnection = new webConnection();
		
		
		while ( webConnection.getTableSize(webConnection.TABLE_EXECUTED) < numberOfOutStandingBids ){
			
			try {
				Thread.sleep(5000);
				System.out.println("Waiting for Confirmation on " + numberOfOutStandingBids + " Bids, Currently " + webConnection.getTableSize(webConnection.TABLE_EXECUTED) + " Confirmed" );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		// Remove executed table:
		// ----------------------
		
		for (Bid nextBid : bidsList){
			webConnection.sendDeleteBidRequest(nextBid);
		}
		
		// Return Same bidList
		return bidsList;
	}
	
	

}
