package com.idanhahn.confirmation;

import java.util.LinkedList;

import com.idanhahn.beans.Bid;

public interface HumanIF {

	public Bid getConfirmation(Bid requestBid);
	
	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList);
}
