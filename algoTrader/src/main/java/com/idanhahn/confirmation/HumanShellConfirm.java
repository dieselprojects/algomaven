package com.idanhahn.confirmation;

import java.util.LinkedList;
import java.util.Scanner;

import com.idanhahn.beans.Bid;

public class HumanShellConfirm implements HumanIF{

	public HumanShellConfirm(){
		System.out.println("Creating Human Shell confirmation method");
	}
	
	public Bid getConfirmation(Bid requestBid) {
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Ready to Bid recieve confirmation:");
		System.out.println("----------------------------------");
		System.out.println(requestBid.print());
		System.out.print("[Approve(Y/y) : DisApprove(N/n)] : ");
		
		
		String response = s.next();
		
		Boolean approved = (response.contains("Y") || response.contains("y")) ? true : false;

		
		if (approved){
			// Update Bid:
			// -----------
			requestBid.approvedHuman = true;
			// Can change bid request Here.
			
		}

		
		return requestBid;		
	}	
	
	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList){
		
		LinkedList<Bid> approvedBids = new LinkedList<Bid>();
		
		for(Bid nextBid : bidsList){
			
			Bid returnedBid = getConfirmation(nextBid);
			
			if (returnedBid.approvedHuman == true){
				approvedBids.addLast(returnedBid);
			}
		
		}
		
		return approvedBids;		
	}

}
