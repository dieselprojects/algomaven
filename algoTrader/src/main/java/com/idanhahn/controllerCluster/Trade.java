package com.idanhahn.controllerCluster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import com.idanhahn.algoCluster.AllAlgo;
import com.idanhahn.beans.StockBean;
import com.idanhahn.beans.Tick;
import com.idanhahn.brokerCluster.BrokerIF;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.confirmation.HumanIF;
import com.idanhahn.dataBase.DataBaseController;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.Types.BrokerTickType;

public abstract class Trade {	
	
	DataBaseController 	databaseController;
	BrokerIF			broker;
	AllAlgo				algo;
	HumanIF				humanIF;
	FundsMan			funds;
	Logger				logger;
	Configuration		configuration;
	
	public Trade(	DataBaseController 	databaseController,
					BrokerIF			broker,
					HumanIF				humanIF,
					FundsMan			funds,
					Logger				logger){
		
		System.out.println("__Constructor__ Trade Abstract");
		this.databaseController = databaseController;
		this.broker				= broker			;           
		this.humanIF			= humanIF			;           
		this.funds				= funds				;            
		this.logger				= logger			;
		configuration = Configuration.getInstance();
	}
	
	
	// ------------------ //
	// - StartIteration - //
	// ------------------ //
	
	public ArrayList<StockBean> startIteration(BrokerTickType brokerTickType){
		
		// Phases:
		// -------
		
		// pre-Trade
		preTrade(brokerTickType);
		
		// Grade
		LinkedList<StockBean> sortedGradeStocks = Grade();
		
		Collections.reverse(sortedGradeStocks);
		
		// trade
		ArrayList<StockBean> tradedStockList = trade(sortedGradeStocks);
		
		// post-Trade
		postTrade(tradedStockList);
		
		
		return tradedStockList;
		
		
		
	}
		
	// ------------- //
	// - Pre-Trade - //
	// ------------- //
	
	// General:
	// update Internal DataBase, using Broker and DataBase controller
	// Input: what kind of update - daily or price
	// Output: Vector of successes updates.
	
	// Body: create ArrayList of 
	
	public void preTrade(BrokerTickType brokerTickType){
		
		// Call DataBase controller and get an arrayList of all tickers in DataBase:
		// -------------------------------------------------------------------------
		ArrayList<String> tickerList = databaseController.getAllStocksBean();
		
		
		// Call Broker with tickerList:
		// ----------------------------
		LinkedList<Tick> tickList = broker.getTicks(tickerList);
		
		
		
		// Update dataBase with Tick information:
		// --------------------------------------
		// get DataBase Bean Vector according to TickerList
		// Update eachBean with new Prices.
		// Write Back Beans
		
		databaseController.updateBeanPrices(tickList);	

	}
	
	// --------- //
	// - Grade - //
	// --------- //
	
	// General:
	// grade each stock in the DataBase using Algo, return sorted LinkedList of all Stocks above a certain 
	// Grade.
	// Input:
	// output: Sorted LinkedList
	
	public abstract LinkedList<StockBean> Grade();
	
	
	// --------- //
	// - Trade - //
	// --------- //
	
	// General:
	// Trade stocks, buy or sell depending on which is the trade implementor
	// input: None (uses the DataBase handle)
	// Output: ArrayList of beans that had been trade ( those been should be removed from DataBase and 
	
	public abstract ArrayList<StockBean> trade(LinkedList<StockBean> gradedStocks);
	
	
	// -------------- //
	// - Post Trade - //
	// -------------- //
	
	// Removing Stocklist from currentDataBase:
	// If Currentlly trading general stocks, this is an indication of buying
	
	public void postTrade(ArrayList<StockBean> stockList){
		
		databaseController.removeStockBean(stockList);
		
	}
	
	public String log(){
		return logger.print();
	}
	
	public Boolean stockExist(){
		 return (databaseController.dataBaseSize() > 0) ? true : false;
	}
	
}
