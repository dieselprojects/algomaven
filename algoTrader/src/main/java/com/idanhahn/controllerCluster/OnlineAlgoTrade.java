package com.idanhahn.controllerCluster;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.idanhahn.algoCluster.AlgoIF;
import com.idanhahn.beans.StockBean;
import com.idanhahn.beans.Tick;
import com.idanhahn.brokerCluster.BrokerIF;
import com.idanhahn.brokerCluster.OnlineBroker;
import com.idanhahn.brokerCluster.TWSBroker;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.confirmation.AutoConfirm;
import com.idanhahn.confirmation.HumanIF;
import com.idanhahn.confirmation.HumanShellConfirm;
import com.idanhahn.confirmation.WindowHumanConfirm;
import com.idanhahn.confirmation.webConfirm;
import com.idanhahn.dataBase.DataBaseController;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.Types.BrokerTickType;
import com.idanhahn.supportCluster.advTimeMan;
import com.idanhahn.supportCluster.webConnection;

public class OnlineAlgoTrade {
	
	// ------------------- //
	// - Class Variables - //
	// ------------------- //
	
	
	// Controller cluster components:
	// ------------------------------
	
	TradeHolding 		tradeHolding;
	DataBaseController	databaseControllerHolding;
	
	TradeGeneral 		tradeGeneral;
	DataBaseController 	databaseControllerGeneral;
		
		
	// Algorithm Cluster components:
	// -----------------------------
	
	AlgoIF mainAlgo;
	
		
	// Broker cluster Components:
	// --------------------------
	
	//SimulatedBroker Broker;
	BrokerIF broker;
	
	// Support cluster components:
	// ---------------------------
	
	FundsMan 			funds;
	HumanIF 			humanConfirm;
	advTimeMan			timeMan;
	Configuration		configuration;
	Logger				logger;
	webConnection		webConnection;	
	
	
	// --------------- //
	// - Constructor - //
	// --------------- //
	
	public OnlineAlgoTrade(){
		
		System.out.println("__Constructor__ Online algotrader");
				
		// Support modules:
		// ----------------
		
		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

		System.out.println("Creating Funds Manager Module with Initial funds of " + configuration.initialFunds);
		funds = FundsMan.getInstance();   	
		switch (configuration.confirmationType){
		case AUTO_CONFIRMATION:
			System.out.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
        	System.err.println("Not valid confirmationType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
			break;
		}	
		
		System.out.println("Creating Time Manager Module");
		timeMan = new advTimeMan();
		
		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();
		
		// Broker Cluster:
		// ---------------
		// can only be ONLINE_BROKER or TWS_ONLINE_BROKER 
		switch (configuration.brokerType){
		case ONLINE_BROKER:
			System.out.println("Creating Broker module, Online Broker");
			broker = OnlineBroker.getInstance();	
			break;
		case TWS_ONLINE_BROKER:
			System.out.println("Creating Broker module, Online Broker");
			broker = TWSBroker.getInstance();	
			break;
		default:
        	System.err.println("Not valid brokerType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
			break;
		}
		
		// Controller Cluster:
		// -------------------

		System.out.println("Creating DataBase Controller for Holding Stocks");
		databaseControllerHolding = new DataBaseController("HoldingDBController");
		
		System.out.println("Creating DataBase Controller for General Stocks");
		databaseControllerGeneral =  new DataBaseController("GeneralDBController");
				
		System.out.println("Creating Trade Holding - for Selling Stocks");
		tradeHolding = new TradeHolding(databaseControllerHolding, broker, humanConfirm, funds, logger);
		
		System.out.println("Creating Trade General - for buying Stocks");
		tradeGeneral = new TradeGeneral(databaseControllerGeneral, broker, humanConfirm, funds, logger);	
		
		
		System.out.println("Finished Simulated AlgoTrade Constructor");	
		
	}
	
	
	public void AlgoStarter(boolean firstStart){
		
		System.out.println("___Start AlgoTrader___");
		
		
		System.out.println("******************************************");
		System.out.println("**    ONLINE ALGOTRADER - SETUP PHASE   **");
		System.out.println("******************************************");		
		logger.addEvent("__ONLINE ALGOTRADER__ AlgoStarter: setup phase");
				
		Setup(firstStart);
		
		System.out.println("****************************************");
		System.out.println("**  ONLINE ALGOTRADER - TRADE PHASE   **");
		System.out.println("****************************************");
		logger.addEvent("__ONLINE ALGOTRADER__ AlgoStarter: trade phase");
		
		Trade();
			
	}
	
	
	// --------- //
	// - Setup - //
	// --------- //
	
	private void Setup(boolean firstStart){
		
		
		// If Exist, Read Saved DataBase.
		// If it's firstStart or no DataBase exist Enter onlineSimulationPhase
		
		
		// Checking if setup/recovery dataBase exists:
		boolean existDataBase = checkSetupDataBase();
		
		if (firstStart == false && existDataBase == true ){
			//Using recoveryDataBase
			try {
				databaseControllerGeneral.loadDataBase(configuration.storeDBFile);
			} catch (NumberFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(2);
			}
		
		} else {
			// Stock Data accumulation, No trade
			setupDataBase();
			
			//Store DataBase
			databaseControllerGeneral.storeDataBase(configuration.storeDBFile);
			System.out.println(databaseControllerGeneral.print());
		}		
	}
	
	
	// --------- //
	// - Trade - //
	// --------- //
	
	public void Trade(){
		
		boolean isDailyTick;
		ArrayList<StockBean> selledstocks = new ArrayList<StockBean>();
		ArrayList<StockBean> boughtStocks;
		
		while ( true ) {
			
			isDailyTick = timeMan.waitUntilNextTick();
			
			String isDailyTickString = ( isDailyTick == true ) ? "DailyTick" : "priceTick";
			
			String nextEventStr = "\nTRADE PHASE, " + timeMan.getCurrentTime() + " , "
					+ isDailyTickString + "\n";
			logger.addEvent(nextEventStr);
			
			if (isDailyTick == true){
							
				System.out.println("Starting trade holding on daily tick:");
				System.out.println("-------------------------------------");
				logger.addEvent("Starting trade holding:");
				
				System.out.println("Starting Trade Holding:");
				System.out.println("-----------------------");
				if (tradeHolding.stockExist() == true)
					selledstocks = tradeHolding.startIteration(BrokerTickType.DailyTick);

				System.out.println("Starting trade general on daily ticl:");
				System.out.println("-----------------------");
				logger.addEvent("Starting Trade General:");
				
				boughtStocks = tradeGeneral.startIteration(BrokerTickType.DailyTick);
			
			} else {
				
				System.out.println("Starting trade holding:");
				System.out.println("-----------------------");
				logger.addEvent("Starting trade holding on price tick");
				
				if (tradeHolding.stockExist() == true)
					selledstocks = tradeHolding.startIteration(BrokerTickType.PriceTick);

				System.out.println("Starting trade general:");
				System.out.println("-----------------------");
				logger.addEvent("Starting trade general on price tick");
				
				boughtStocks = tradeGeneral.startIteration(BrokerTickType.PriceTick);				
								
			}
			// TODO, planning problem trade's should not be treated as DataBase controllers
			// tradeHolding/generalHolding
			// Also, add StopLost to algorithm
			// Add minimum iteration before sell bought stock.
			
			databaseControllerHolding.addStockBeans(boughtStocks);
			
			if (tradeHolding.stockExist() == true)
				databaseControllerGeneral.addStockBeans(selledstocks);
			
		}
		
	}
	
	
	// setup DataBase with simulated online trade:
	private void setupDataBase(){
	
		// Get filtered stock list:
		ArrayList<String> tickerArray = broker.getAllTickers(true);
		
		// Create DataBase in setup phase:
		for (int i = 0 ; i < configuration.setupTicks; i++){
			System.out.println("inside setup phase, waiting for next tick");
			System.out.println("setup phase tick remaing: " + (configuration.setupTicks - i));
			Boolean isDailyTick = timeMan.waitUntilNextTick();
			
			// getting first ticks from Broker
			LinkedList<Tick> ticksArray = broker.getTicks(tickerArray);
			// Adding new stocks to DataBase by tick array
			//databaseControllerGeneral.addStockBeans(ticksArray);
			System.err.println("TODO: FIX onlineAlgo");
			System.exit(1);
		}
		
	}
	
	
	private Boolean checkSetupDataBase(){
		
		File f = new File(configuration.storeDBFile);
		return f.isFile();
		
	}
}
