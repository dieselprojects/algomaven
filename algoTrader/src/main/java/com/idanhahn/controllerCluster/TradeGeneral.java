package com.idanhahn.controllerCluster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import com.idanhahn.algoCluster.AllAlgo;
import com.idanhahn.beans.Bid;
import com.idanhahn.beans.StockBean;
import com.idanhahn.brokerCluster.BrokerIF;
import com.idanhahn.brokerCluster.SimulatedBroker;
import com.idanhahn.confirmation.HumanIF;
import com.idanhahn.dataBase.DataBaseController;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.Types.OPType;

public class TradeGeneral extends Trade {

	public TradeGeneral(DataBaseController databaseController, BrokerIF broker,
			HumanIF humanIF, FundsMan funds, Logger logger) {
		super(databaseController, broker, humanIF, funds, logger);
		System.out.println("__Constructor__ TradeGeneral");
		// algo = new AllAlgo(OPType.Buy);
	}

	// --------- //
	// - Grade - //
	// --------- //

	// General:
	// 3. Grade - Grade all stocks in the DataBase and return a Sorted
	// Linkedlist of StockGrade with Grades.
	// Input:
	// output: Sorted LinkedList with grade high enough for trade

	public LinkedList<StockBean> Grade() {

		LinkedList<StockBean> sortedGradeStocks = new LinkedList<StockBean>();
		Double minimumGrade = configuration.minGradeBuy;

		// Get all stock from database controller:
		ArrayList<StockBean> allStocks = databaseController.getAllStockBean();

		for (StockBean nextStock : allStocks) {

			// creating thread for each stock and grade all stocks concurrently
			algo = new AllAlgo(nextStock, OPType.BUY);
			algo.run();
			nextStock.grade = algo.getGrade();
			logger.addEvent(algo.printAlgoResults());

			System.out.print(algo.printAlgoResults());

			// if stock received enough high grade add it to next level queue
			// for more processing
			if (nextStock.grade >= minimumGrade) {
				sortedGradeStocks.add(nextStock);
			}
		}

		Collections.sort(sortedGradeStocks);
		return sortedGradeStocks;

	}

	// --------- //
	// - TRADE - //
	// --------- //
	// 4. Trade - Input GradedList, Phases:
	// a. Create Bids LinkedList, each Bid contain price, amount to Buy, type
	// and the Stock
	// b. HumanIf gets the Bids Vector, request approval/edits and return
	// LinkedList of approvedBids (same as bids)
	// c. broker gets approvedBids and place Bids online- Can take time. -
	// returns DoneBids vector. (some can not be done)
	// d. funds get the DoneBids and removeFunds according to the doneBids
	// e. funds use DoneBid to update the doneBid.Stock
	// f. funds Logs events and returns the doneBids.
	// g. Under Trade, Stock is taken from doneBids and placed in
	// ArrayList<Stocks> tradedStocksList -> Output value

	public ArrayList<StockBean> trade(LinkedList<StockBean> sortedGradeStocks) {

		// a. Create Bids LinkedList, each Bid contain price, amount to sell,
		// type and the Stock
		LinkedList<Bid> BidsList = new LinkedList<Bid>();
		Double tempFunds = funds.getCurrentFunds();

		for (StockBean nextBuyStock : sortedGradeStocks) {

			// Currently Buy 10% of
			// TODO: add strategy Support module

			Double fundsToBuy = tempFunds;
			tempFunds -= fundsToBuy;
			int amountToBuy = (int) Math.floor(fundsToBuy / nextBuyStock.price);

			if (amountToBuy > 5) {

				if (broker.getClass() == SimulatedBroker.class)
					nextBuyStock.iterationTrade = ((SimulatedBroker) broker)
							.getIteration();

				//Bid nextBid = new Bid(nextBuyStock.ticker, nextBuyStock.price,
				//		amountToBuy, OPType.BUY, nextBuyStock);

				//BidsList.addLast(nextBid);
			}
		}

		// b. HumanIf gets the Bids Vector, request approval/edits and return
		// LinkedList of approvedBids (same as bids):

		HumanIF humanConfirm = humanIF;
		LinkedList<Bid> approvedBids = humanConfirm.getConfirmation(BidsList);

		// c. broker gets approvedBids and place Bids online- Can take time. -
		// returns DoneBids vector. (some can not be done)
		// broker also updated Bid.Stock with Buy/Sell parameters:
		// Buy:
		// ----
		// CurrentStatus - true
		// BuyPrice - price
		// BuyGrade
		// Amount
		//
		LinkedList<Bid> doneBids = broker.placeBids(approvedBids);

		// g. Under Trade, Stock is taken from doneBids and placed in
		// ArrayList<Stocks> tradedStocksList -> Output value

		ArrayList<StockBean> tradedStockList = new ArrayList<StockBean>(
				doneBids.size());

		for (Bid nextBid : doneBids) {
			if (nextBid.approvedBroker == true)
				tradedStockList.add(nextBid.stock);
		}

		return tradedStockList;
	}

}
