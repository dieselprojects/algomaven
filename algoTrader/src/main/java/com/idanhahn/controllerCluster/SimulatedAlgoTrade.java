package com.idanhahn.controllerCluster;

import java.util.ArrayList;
import java.util.LinkedList;

import com.idanhahn.algoCluster.AlgoIF;
import com.idanhahn.beans.StockBean;
import com.idanhahn.beans.Tick;
import com.idanhahn.brokerCluster.SimulatedBroker;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.confirmation.AutoConfirm;
import com.idanhahn.confirmation.HumanIF;
import com.idanhahn.confirmation.HumanShellConfirm;
import com.idanhahn.confirmation.WindowHumanConfirm;
import com.idanhahn.confirmation.webConfirm;
import com.idanhahn.dataBase.DataBaseController;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.SimulateTimeMan;
import com.idanhahn.supportCluster.TimeMan;
import com.idanhahn.supportCluster.Types.ConfirmationType;
import com.idanhahn.supportCluster.webConnection;

public class SimulatedAlgoTrade {

	// ------------------- //
	// - Class Variables - //
	// ------------------- //

	// Controller cluster Components:
	// ------------------------------
	SimController simController;
	public DataBaseController dataBaseController;
	
	
	// Algorithm Cluster components:
	// -----------------------------

	AlgoIF mainAlgo;

	// Broker cluster Components:
	// --------------------------

	public SimulatedBroker Broker;

	// Support cluster components:
	// ---------------------------

	FundsMan funds;
	HumanIF humanConfirm;
	TimeMan timeMan;
	SimulateTimeMan simTimeMan;
	Configuration configuration;
	Logger logger;
	webConnection webConnection;

	// --------------- //
	// - Constructor - //
	// --------------- //

	public SimulatedAlgoTrade() {

		// Support modules:
		// ----------------

		System.out.println("__Constructor__ Simulated AlgoTrade");

		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

		switch (configuration.confirmationType) {
		case AUTO_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out
					.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out
					.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
			System.err.println("Not valid confirmationType selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}

		System.out.println("Creating Time Manager Module");
		timeMan = new TimeMan();

		System.out.println("Creating Simulate Time Manager Module");
		simTimeMan = SimulateTimeMan.getInstance();

		// Broker Cluster:
		// ---------------
		System.out.println("Creating Simulated Broker Cluster");
		Broker = SimulatedBroker.getInstance();

		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();

		System.out
				.println("Creating Funds Manager Module with Initial funds of "
						+ configuration.initialFunds);
		funds = FundsMan.getInstance();

		// Controller Cluster:
		// -------------------
		System.out.println("Creating DataBase Controller");
		dataBaseController = new DataBaseController("DataBaseController");
		
		System.out.println("Creating simulate controller");
		simController = new SimController(dataBaseController, Broker, humanConfirm, funds, logger);
		
		System.out.println("Finished Simulated AlgoTrade Constructor");
	}

	public void SimulationStarter() {

		logger.addEvent("*********************************************");
		logger.addEvent("**    Simulatd ALGOTRADER - SETUP PHASE   **");
		logger.addEvent("********************************************");

		Setup();

		logger.addEvent("*******************************************");
		logger.addEvent("**  Simulated ALGOTRADER - TRADE PHASE   **");
		logger.addEvent("*******************************************");

		Trade();

		logger.addEvent("********************************************");
		logger.addEvent("**   Simulated ALGOTRADER - FINAL PHASE   **");
		logger.addEvent("********************************************");

		Final();

	}

	// --------- //
	// - SETUP - //
	// --------- //

	private void Setup() {
		// Add StocksBean to DataBase and add prices,

		// Simulated Algo Setup phase:
		// ---------------------------

		// At this point there's full broker database ready.
		// a. Call Broker and get all stored Tickers.?? not sure if it's should
		// be in the broker class
		// b. use tickers to Call Broker and get DailyTicks
		// c. AdvIteration
		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean
		// f. Use ArrayList of StockBeans to push into the DataBase
		// g. use addStockBean to #setupIteration to GeneralDataBase.

		// ------------------------------------- //
		// -- Specific Additional Setup Tasks -- //
		// ------------------------------------- //

		// Add WebConnection Interface:
		// ----------------------------
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION) {
			webConnection.sendMail();
			webConnection.createMySQL();
		}

		// -------------------------------- //
		// -- End Additional Setup Tasks -- //
		// -------------------------------- //

		// a. Call Broker and get all stored Tickers.?? not sure if it's should
		// be in the broker class
		ArrayList<String> allTickerArr = Broker.getAllTickers(false);

		// b. use tickers to Call Broker and get DailyTicks
		LinkedList<Tick> firstTickArr = Broker.getTicks(allTickerArr);

		// c. AdvIteration
		// Broker.advIteration();

		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean

		ArrayList<StockBean> newStockBeanArr = convertDailyTickToBean(firstTickArr);

		// f. Use ArrayList of StockBeans to push into the DataBase

		dataBaseController.addStockBeans(newStockBeanArr);

		// g. use addStockBean to add all.

		for (int i = 1; i < Configuration.getInstance().setupTicks; i++) {

			LinkedList<Tick> dailyTickArr = Broker.getTicks(Broker
					.getAllTickers(false));
			dataBaseController.updateBeanPrices(dailyTickArr);

			Broker.advIteration();

		}

	}

	// ----------------- //
	// - Trade phase - //
	// ----------------- //

	private void Trade() {

		while (Broker.advIteration()) {

			timeMan.waitUntilNextTick();
			String tradePhase = 			
			"--------------------------------------" + "\n" +
			"--    IN TRADE PHASE, ITERATION "  
					+ Broker.getIteration() + "  --" + "\n" +
			"--  " + simTimeMan.getTimeString() + "  --" + "\n" +
			"--------------------------------------" + "\n";
			logger.addEvent(tradePhase);
			
			dataBaseController.addStockBeans(simController.startIteration());

		}
	}

	// --------- //
	// - FINAL - //
	// --------- //

	private void Final() {

		// for web confirmation, destroy SQL database
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION) {
			webConnection.destroyMySQL();
		}

		if (configuration.enablePrintToScreen == true)
			System.out.println(logger.printAll(dataBaseController.getAllStockBean()));
	}

	// Convert Tick to stockBean:
	// --------------------------

	private StockBean convertDailyTickToBean(Tick tick) {
		StockBean bean = new StockBean(tick);
		return bean;
	}

	private ArrayList<StockBean> convertDailyTickToBean(LinkedList<Tick> tickArr) {

		ArrayList<StockBean> beanArr = new ArrayList<StockBean>(tickArr.size());

		for (Tick nextTick : tickArr) {
			beanArr.add(convertDailyTickToBean(nextTick));
		}

		return beanArr;
	}

}
