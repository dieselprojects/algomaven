package com.idanhahn.controllerCluster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.idanhahn.algoCluster.Algorithm;
import com.idanhahn.beans.Bid;
import com.idanhahn.beans.StockBean;
import com.idanhahn.beans.Tick;
import com.idanhahn.brokerCluster.BrokerIF;
import com.idanhahn.configurationCluster.Configuration;
import com.idanhahn.confirmation.HumanIF;
import com.idanhahn.dataBase.DataBaseController;
import com.idanhahn.supportCluster.FundsMan;
import com.idanhahn.supportCluster.Logger;
import com.idanhahn.supportCluster.SimulateTimeMan;

public class SimController {

	DataBaseController databaseController;
	BrokerIF broker;
	Algorithm algorithm;
	HumanIF humanIF;
	FundsMan funds;
	SimulateTimeMan simTimeMan = SimulateTimeMan.getInstance();
	Logger logger;
	Configuration configuration;

	public SimController(DataBaseController databaseController, BrokerIF broker,
			HumanIF humanIF, FundsMan funds, Logger logger) {

		System.out.println("__Constructor__ Trade Abstract");
		this.databaseController = databaseController;
		this.broker = broker;
		this.humanIF = humanIF;
		this.funds = funds;
		this.logger = logger;
		configuration = Configuration.getInstance();
	}

	public ArrayList<StockBean> startIteration(){

		// Get next tick data from broker:
		ArrayList<String> tickerList = databaseController.getAllStocksBean();
		LinkedList<Tick> tickList = broker.getTicks(tickerList);
		databaseController.updateBeanPrices(tickList);	
		
		// Exec algorithm on stocks with new data:
		ArrayList<StockBean> stocksArray = databaseController.getAllStockBean();
		algorithm = new Algorithm(stocksArray);
		LinkedList<Bid> algoBidList = algorithm.start(simTimeMan.getTime());

		// send human confirm:
		LinkedList<Bid> humanConfirmedBids = humanIF.getConfirmation(algoBidList);
		
		// Send algorithm bid list to broker for exec:
		LinkedList<Bid> doneBids = broker.placeBids(humanConfirmedBids);
		
		ArrayList<StockBean> returnStockList = new ArrayList<StockBean>();
		
		Iterator<Bid> iterator = doneBids.iterator();
		while(iterator.hasNext()){
			returnStockList.add(iterator.next().stock);
		}
	
		return returnStockList;
		
	}
	
	
}
