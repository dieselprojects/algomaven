package com.idanhahn.configurationCluster;

import java.util.LinkedList;

import com.tictactec.ta.lib.MAType;

public class MACDCfg {

	private static final MACDCfg MACDCfg = new MACDCfg();

	// Public static parameters:
	// -------------------------
	public static int macdSimTicksParam = 30;
	public static int macdFastParam = 12;
	public static int macdSlowParam = 26;
	public static int macdSignalParam = 9;
	public static int macdTickBeforeParam = 3;
	public static double macdIndicaitonMargain = 0.1;
	public static MAType macdMATypeFastParam = MAType.Ema;
	public static MAType macdMATypeSlowParam = MAType.Ema;
	public static MAType macdMATypeSignalParam = MAType.Ema;

	public static MACDCfg getInstance() {
		return MACDCfg;
	}

	public void print() {
		System.out.println("MACD configuration:");
		System.out.println("\tmacdSimTicksParam     : " + macdSimTicksParam);
		System.out.println("\tmacdFastParam         : " + macdFastParam);
		System.out.println("\tmacdSlowParam         : " + macdSlowParam);
		System.out.println("\tmacdSignalParam       : " + macdSignalParam);
		System.out.println("\tmacdTickBeforeParam   : " + macdTickBeforeParam);
		System.out
				.println("\tmacdIndicaitonMargain : " + macdIndicaitonMargain);
		System.out.println("\tmacdMATypeFastParam   : " + macdMATypeFastParam);
		System.out.println("\tmacdMATypeSlowParam   : " + macdMATypeSlowParam);
		System.out
				.println("\tmacdMATypeSignalParam : " + macdMATypeSignalParam);
		System.out.println("-------------------------------");
	}
	
	public LinkedList<String> getValues() {

		LinkedList<String> list = new LinkedList<String>();

		list.addLast("MACD configuration:");
		list.addLast("macdSimTicksParam     : " + macdSimTicksParam);
		list.addLast("macdFastParam         : " + macdFastParam);
		list.addLast("macdSlowParam         : " + macdSlowParam);
		list.addLast("macdSignalParam       : " + macdSignalParam);
		list.addLast("macdTickBeforeParam   : " + macdTickBeforeParam);
		list.addLast("macdIndicaitonMargain : " + macdIndicaitonMargain);
		list.addLast("macdMATypeFastParam   : " + macdMATypeFastParam);
		list.addLast("macdMATypeSlowParam   : " + macdMATypeSlowParam);
		list.addLast("macdMATypeSignalParam : " + macdMATypeSignalParam);
		list.addLast("-------------------------------");
		return list;
	}
	
	
}
